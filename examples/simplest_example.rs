use bevy::input::common_conditions::input_toggle_active;
use bevy::prelude::*;
use bevy_bunny_textelitext_400::FontWithTextureArray;
use bevy_inspector_egui::quick::WorldInspectorPlugin;
use bevy_bunny_textelitext_400::TextMaterial;
use bevy_bunny_textelitext_400::TextMaterialPlugin;

fn main() {
    println!("Hello, bunnies!");
    App::new()
        .add_plugins(DefaultPlugins)
        .add_plugins(MaterialPlugin::<TextMaterial>::default())
        .add_plugins(TextMaterialPlugin)
    
        .add_plugins(WorldInspectorPlugin::default()
            .run_if(input_toggle_active(false, KeyCode::Escape)))
        
        .add_systems(Startup, spawn_spawnies)
        .add_systems(Update, rotate_those)

        .run()
        ;
}

#[derive(Component)]
struct RotateThis;

fn spawn_spawnies(
    mut commands: Commands,
    mut material_assets: ResMut<Assets<TextMaterial>>,
    mut mesh_assets: ResMut<Assets<Mesh>>,
    mut font_w_texture_assets: ResMut<Assets<FontWithTextureArray>>,
) {
    commands.spawn((
        Name::from("Camera"),
        Camera3dBundle {
            transform: Transform::default()
                .with_translation(Vec3::new(1.0, 1.0, 7.0))
                .looking_at(Vec3::ZERO, Vec3::Y),
            ..default()
        },
    ));

    let mesh = Mesh::from(shape::Cube{ ..default() });

    let font_with_textures = FontWithTextureArray
        ::new(100, 100, false, None, None, None, Some('#'));

    let mut material = TextMaterial::new(
        font_w_texture_assets.add(font_with_textures)
    );
    
    material.set_text("UwUOwO".chars().collect(), 3, 2);

    commands.spawn((
        Name::from("Text Cube"),
        RotateThis{},
        MaterialMeshBundle{
            material: material_assets.add(material),
            mesh: mesh_assets.add(mesh.clone()),
            ..default()
        }
    ));
}

fn rotate_those(mut those: Query<&mut Transform, With<RotateThis>>, time: Res<Time>)
{
    let axis = Vec3::ONE.normalize();
    for mut t in those.iter_mut() {
        t.rotate_axis(axis, time.delta_seconds());
        t.rotation = t.rotation.normalize();
    }
}

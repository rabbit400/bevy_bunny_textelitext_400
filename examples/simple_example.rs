use bevy::input::common_conditions::input_toggle_active;
use bevy::prelude::*;
use bevy::render::texture::ImageAddressMode;
use bevy::render::texture::ImageFilterMode;
use bevy::render::texture::ImageSampler;
use bevy::render::texture::ImageSamplerDescriptor;
use bevy_bunny_textelitext_400::FontWithTextureArray;
use bevy_inspector_egui::bevy_egui::EguiContexts;
use bevy_inspector_egui::egui;
use bevy_inspector_egui::quick::WorldInspectorPlugin;
use bevy_bunny_textelitext_400::TextMaterial;
use bevy_bunny_textelitext_400::TextMaterialPlugin;

fn main() {
    println!("Hello, bunnies!");
    App::new()
        .add_plugins(DefaultPlugins)
        .add_plugins(MaterialPlugin::<TextMaterial>::default())
        .add_plugins(TextMaterialPlugin)
    
        .add_plugins(WorldInspectorPlugin::default()
            .run_if(input_toggle_active(false, KeyCode::Escape)))
        
        .init_resource::<RuntimeSettings>()
        .init_resource::<ConstructionSettings>()
        .init_resource::<PreviousSettings>()

        .add_systems(Startup, spawn_spawnies)
        .add_systems(Update, rotate_those)
        .add_systems(Update, gui)
        .add_systems(Update, update_text_material)

        .run();
}

#[derive(Resource, Clone, PartialEq)]
struct RuntimeSettings {
    row_length: String,
    num_rows: String,
    text: String,
    text_colour: [f32; 4],
    bg_colour: [f32; 4],
}
impl Default for RuntimeSettings {
    fn default() -> Self { Self { 
        row_length: "4".into(),
        num_rows: "2".into(),
        text: "UwU  OwO".chars().collect(),
        text_colour: Color::BLACK.as_rgba_f32(),
        bg_colour:  Color::LIME_GREEN.as_rgba_f32(),
    } }
}

#[derive(Resource, Clone, PartialEq)]
struct ConstructionSettings {
    width: String,
    height: String,
    keep_aspect_ratio: bool,
    coverage_cutoff: String,
    default_char: String,
    font_name: String,
    sampler: SamplerOptions,
}
impl Default for ConstructionSettings {
    fn default() -> Self { Self {
         width: "100".into(),
         height: "100".into(),
         keep_aspect_ratio: false,
         coverage_cutoff: "".into(),
         default_char: "#".into(),
         font_name: "".into(),
         sampler: default(),
    } }
}
#[derive(Resource, Default)]
struct PreviousSettings
{
    runtime_settings: Option<RuntimeSettings>,
    construction_settings: Option<ConstructionSettings>,
}

fn gui(
    mut contexts: EguiContexts,
    mut runtime_settings: ResMut<RuntimeSettings>,
    mut construction_settings: ResMut<ConstructionSettings>,
) {
    egui::Window::new("Hello").show(contexts.ctx_mut(), |ui| {
        ui.vertical(|ui|{
            ui.label("construction settings")
                .on_hover_text(ON_HOOVER_CONSTRUCTION_SETTINGS);
            gui_for_construction_settings(ui, &mut construction_settings);
            ui.separator();
            ui.label("runtime settings")
                .on_hover_text(ON_HOOVER_RUNTIME_SETTINGS);
            gui_for_runtime_settings(ui, &mut runtime_settings);
        });
    });
}

fn gui_for_construction_settings(ui: &mut egui::Ui, cs: &mut ConstructionSettings)
{
    ui.horizontal(|ui|{
        ui.vertical(|ui|{
            ui.horizontal(|ui|{
                ui.set_max_width(100.);
                ui.label("width")
                    .on_hover_text(ON_HOVER_WIDTH);
                ui.text_edit_singleline(&mut cs.width);
            });
            ui.horizontal(|ui|{
                ui.set_max_width(100.);
                ui.label("height")
                    .on_hover_text(ON_HOVER_HEIGHT);
                ui.text_edit_singleline(&mut cs.height);
            });
            ui.horizontal(|ui|{
                ui.set_max_width(100.);
                ui.label("coverage_cutoff")
                    .on_hover_text(ON_HOVER_COVERAGE_CUTOFF);
                ui.text_edit_singleline(&mut cs.coverage_cutoff);
            });
        });
        ui.vertical(|ui|{
            ui.horizontal(|ui|{
                ui.checkbox(&mut cs.keep_aspect_ratio, "meh")
                    .on_hover_text(ON_HOOVER_MEH_AKA_KEEP_ASPECT_RATION);
                ui.label("default_character")
                    .on_hover_text(ON_HOVER_DEFAULT_CHARACTER);
                ui.text_edit_singleline(&mut cs.default_char);
            });
            ui.horizontal(|ui|{
                ui.label("font_name")
                    .on_hover_text(ON_HOVER_FONT_NAME);
                ui.text_edit_singleline(&mut cs.font_name);
            });
            egui::ComboBox::from_label("<- sampler")
                .selected_text(cs.sampler.to_string())
                .show_ui(ui, |ui| {
                    for so in SamplerOptions::values() {
                        ui.selectable_value(&mut cs.sampler, *so, so.to_string());
                    }
                });
        });
    });
}

fn gui_for_runtime_settings(ui: &mut egui::Ui, rs: &mut RuntimeSettings)
{
    ui.horizontal(|ui|{
        ui.vertical(|ui|{
            ui.horizontal(|ui|{
                ui.set_max_width(100.);
                ui.label("row length");
                ui.text_edit_singleline(&mut rs.row_length);
            });
            ui.horizontal(|ui|{
                ui.set_max_width(100.);
                ui.label("num rows");
                ui.text_edit_singleline(&mut rs.num_rows);
            });
        });
        ui.vertical(|ui|{
            ui.label("text");
            ui.color_edit_button_rgba_unmultiplied(&mut rs.text_colour);
        });
        ui.vertical(|ui|{
            ui.label("background");
            ui.color_edit_button_rgba_unmultiplied(&mut rs.bg_colour);
        });
    });
    ui.horizontal(|ui|{
        ui.label("text");
        ui.text_edit_singleline(&mut rs.text);
    });
}

fn update_text_material(
    construction_settings: ResMut<ConstructionSettings>,
    runtime_settings: ResMut<RuntimeSettings>,
    mut previous: ResMut<PreviousSettings>,
    mut text_materials: ResMut<Assets<TextMaterial>>,
    mut text_mat_handles: Query<&mut Handle<TextMaterial>>,
    asset_server: Res<AssetServer>,
    mut font_w_textures_assets: ResMut<Assets<FontWithTextureArray>>,
) {
    if match &previous.construction_settings {
        None => true,
        Some(prev) => *prev != *construction_settings
    } {
        previous.construction_settings = Some(construction_settings.clone());
        previous.runtime_settings = None;

        let width = parse_width_or_height(&construction_settings.width);
        let height = parse_width_or_height(&construction_settings.height);
        // note that next() can return None
        let default_char = construction_settings.default_char.chars().next();
        let keep_aspect = construction_settings.keep_aspect_ratio;
        let coverage_cutoff = construction_settings.coverage_cutoff.parse::<f32>().ok();
        let font = match construction_settings.font_name.trim() {
            "" => None,
            n => Some(asset_server.load(n.to_string())),
        };
        let sampler = construction_settings.sampler.to_sampler_option();

        let font_with_textures = FontWithTextureArray::new(
            width, height, keep_aspect, coverage_cutoff, font, sampler, default_char);
        let new_mat = TextMaterial::new(font_w_textures_assets.add(font_with_textures));
        let new_mat_handle = text_materials.add(new_mat);

        for mut h in text_mat_handles.iter_mut() {
            *h = new_mat_handle.clone();
        }
    }

    if match &previous.runtime_settings {
        None => true,
        Some(prev) => *prev != *runtime_settings
    } {
        previous.runtime_settings = Some(runtime_settings.clone());

        for (_, m) in text_materials.iter_mut() {
            m.set_text(runtime_settings.text.chars().collect(),
                runtime_settings.row_length.trim().parse().unwrap_or(1),
                runtime_settings.num_rows.trim().parse().unwrap_or(1),
            );
            let fg = &runtime_settings.text_colour;
            let bg = &runtime_settings.bg_colour;
            m.set_text_colour(Color::rgba(fg[0], fg[1], fg[2], fg[3]));
            m.set_background_colour(Color::rgba(bg[0], bg[1], bg[2], bg[3]));
        }
    }
}

fn parse_width_or_height(s: &str) -> u8 {
    match s.parse::<u8>() {
        Ok(0) => 1,
        Ok(n) => n,
        Err(_) => 1,
    }
}

#[derive(Component)]
struct RotateThis;

fn spawn_spawnies(
    mut commands: Commands,
    mut mesh_assets: ResMut<Assets<Mesh>>,
) {
    commands.spawn((
        Name::from("Camera"),
        Camera3dBundle {
            transform: Transform::default()
                .with_translation(Vec3::new(1.0, 1.0, 7.0))
                .looking_at(Vec3::ZERO, Vec3::Y),
            ..default()
        },
    ));

    let mesh = Mesh::from(shape::Cube{ ..default() });

    commands.spawn((
        Name::from("Text Cube"),
        RotateThis{},
        MaterialMeshBundle{
            material: Handle::<TextMaterial>::default(),
            mesh: mesh_assets.add(mesh.clone()),
            // default transform works fine... these randomish changes makes it slightly
            //      nicer to put several app windows next to each other.
            transform: Transform::from_translation(Vec3::new(0.0, -1.0, 0.0))
                        .with_scale(2.0 * Vec3::ONE),
            ..default()
        }
    ));
}

fn rotate_those(mut those: Query<&mut Transform, With<RotateThis>>, time: Res<Time>)
{
    let axis = Vec3::ONE.normalize();
    for mut t in those.iter_mut() {
        t.rotate_axis(axis, time.delta_seconds());
        t.rotation = t.rotation.normalize();
    }
}

// this used to be "const NEAREST_SAMPLER: ImageSamplerDescriptor = ..." in bevy 0.11
fn nearest_sampler() -> ImageSamplerDescriptor {
    ImageSamplerDescriptor {
        label: Some("NEAREST SAMPLER".into()),
        address_mode_u: ImageAddressMode::ClampToEdge,
        address_mode_v: ImageAddressMode::ClampToEdge,
        address_mode_w: ImageAddressMode::ClampToEdge,
        mag_filter: ImageFilterMode::Nearest,
        min_filter: ImageFilterMode::Nearest,
        mipmap_filter: ImageFilterMode::Nearest,
        lod_min_clamp: 0.0,
        lod_max_clamp: 32.0,
        compare: None,
        anisotropy_clamp: 1,
        border_color: None,
    }
}

#[derive(Clone, Copy, PartialEq, Debug, Default)]
enum SamplerOptions {
    #[default]
    TextureDefaultSampler,
    SamplerDefaults,
    NearestSampler
}
impl SamplerOptions {
    fn values() -> std::slice::Iter<'static, SamplerOptions> {
        [Self::TextureDefaultSampler, Self::SamplerDefaults, Self::NearestSampler].iter()
    }
    fn to_sampler_option(self: &Self) -> Option<ImageSampler> {
        match self {
            SamplerOptions::TextureDefaultSampler => None,
            SamplerOptions::SamplerDefaults => Some(ImageSampler::Descriptor(ImageSamplerDescriptor::default())),
            SamplerOptions::NearestSampler => Some(ImageSampler::Descriptor(nearest_sampler())),
        }
    }
}
impl std::fmt::Display for SamplerOptions {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        std::fmt::Debug::fmt(self, f)
    }
}


const ON_HOOVER_CONSTRUCTION_SETTINGS: &str = 
"construction_settings
            These settings are not meant to change a lot.
            Changing these will cause recreation of the
            FontWithTextures struct.";
const ON_HOOVER_RUNTIME_SETTINGS: &str = 
"runtime_settings
            These settings are meant to change during normal runtime.
            Text changes can cause generation of character textures
            - but ONLY if there are new characters not seen since
            [re]creation of the FontWithTextures struct.
            (Currently that regenerates all character textures but
            this is easily improved.)";
const ON_HOOVER_MEH_AKA_KEEP_ASPECT_RATION: &str = 
"meh (aka keep_aspect_ratio)
            This is whether a character keeps its aspect ratio when rendered into its image.
            NOTE that the image is stretched in the shader so aspect ratio is NOT kept on
            the materials final visual. 'meh' is less confusing name than 'keep aspect ratio'.
            (I guess I should delete this setting but im still emotionally attached.)";
const ON_HOVER_DEFAULT_CHARACTER: &str =
"default_character
            The character shown as filler when
              - the text is to short to cover all the shader's area
              - the character in the text doesnt work for some reason
                (though currently its more likely the program will crash at some unwrap)
            When parsing the ui string we only look at FIRST CHARACTER or if string is EMPTY.
            Empty string maps to 'None' which means you probably dont care. In this case
            the first character seen by the materials 'FontWithTextures' property is
            used for default_caracter.";
const ON_HOVER_FONT_NAME: &str =
"font_name
            Name of your custom font. It will be loaded with asset_server.load(font_name).
            Putting them in the assets folder OR typing out the full path works on my machine.
            Default bevy font is used if this field is empty (or only whipespace).
            (I tested with the bevy assets fonts 'FiraSans-Bold.ttf' and 'FiraMono-Medium.ttf'.)";
const ON_HOVER_WIDTH: &str =
"width
            Width in pixels on image generated for each character.
            The image is stretched by the shader so this is not a size setting
            - rather a resolution setting.";
const ON_HOVER_HEIGHT: &str =
"height
            Height in pixels on image generated for each character.
            The image is stretched by the shader so this is not a size setting
            - rather a resolution setting.";
const ON_HOVER_COVERAGE_CUTOFF: &str =
"coverage_cutoff
            empty or f32 in [0.0 1.0]
            if set: 
                a pixel will be 1.0 or 0.0 depending on if the coverage
                of that pixel is above or below the limit.
            if empty (or invalid f32):
                the pixel value will be the (rounded) coverage value
                since we currently use R8norm textures value will be
                in the list: (0, 1/255, ... , 255/255)
            * coverage is a measure in [0.0 1.0] of how much a pixel
                covered by that font character";
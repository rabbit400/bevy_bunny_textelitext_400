# bevy_bunny_textelitext_400

This is an UwU crate for showing monospace 3d text in bevy OwO using a shader, no mesh nor render to texture.

## What and how it does this
Uwuser creates a material 'TextMaterial' and sets the text.
UwU creates a texture array with an image for each character.
OwO shader gets the texture array and the characters and shows the characters.
Awooo is that only the text needs updating when uwuser changes the text.
(unless there are new previously unseen characters in the text.)

It divides the shaded area into rows with same length and puts the text characters 
there in order and with equal size. **This assumes U and V goes from 0 to 1.**

## How to test it

Git-clone this repo and do 'cargo run --example simple_example'

## How to use it

Add it to youwur carg.toml:
```
[dependencies.bevy_bunny_textelitext_400]
git = "https://gitlab.com/rabbit400/bevy_bunny_textelitext_400.git"
```
Add the material plugin to your app:
```
app.add_plugins(MaterialPlugin::<TextMaterial>::default())
```
Create a FontWithTextures and then the material:
(Note the Arc<Mut<.>> part which means youwu should use the same FontWithTextures for several materials if they share the same font and settings.)
```
let font_with_textures = FontWithTextureArray::new(
    width, height, keep_aspect, coverage_cutoff, font, sampler, default_char);
let new_mat = TextMaterial::new(Arc::new(Mutex::new(font_with_textures)));
```
Then put this material on a quad or cube or whatever.
**UV coordinates are assumed to go from 0 to 1.**

If anyone actually use this yowou should probably first clone it to get a stabler git link.
Then tell me how make this dual license MIT Apache.

## What could be done

Specify origo position and width and height in UV coordinates instead of the [0 1] assumptions on UV.

Let characters have different width - i.e. allow non monospace.

Add a function to pre-render (generate texture array elements) a list of characters.
(Currently youd set them as text and wait one frame... luckily the material doesnt need to show so
you could have a 'pre-render-material' using the same backing FontWithTextures.)

Do something with characters that dont have glyph but have an image.
Currently we probably <.< crash >.>

Make this into a normalish PBR material where the text is put on top - emissive or not as a setting.

## What should be done

Redraw only the new characters when recreating the font characters texture array.
For now Im hoping for a bogfix in bevy creating array texture of length 1 cause the 
workaround I use would make this a bit messy.

Handle some unwrap() calls that would crash an app using characters not supported by the font.

The TextMaterial type should be cleaned up. Probably it now runs a lot PBR shader stuff that is not used.

## Which weird setting is the most UwU?
Your vote matters!

### a) width
Width in pixels on image generated for each character.
The image is stretched by the shader so this is not a size setting - rather a resolution setting.

### b) height
Meh, just width but vertical.

### c) coverage_cutoff
empty or f32 in [0.0 1.0]
if set: 
    a pixel will be 1.0 or 0.0 depending on if the coverage
    of that pixel is above or below the limit.
if Nowone
    the pixel value will be the (rounded) coverage value
    since we currently use R8norm textures value will be
    in the list: (0, 1/255, ... , 255/255)
* coverage is a measure in [0.0 1.0] of how much a pixel is
    covered by that font character.

### d) default_char
The character shown as filler when
    - the text is to short to cover all the shader's area
    - the character in the text doesnt work for some reason
    (though currently its more likely the program will crash at some unwrap)
When parsing the ui string we only look at FIRST CHARACTER or if string is EMPTY.
Empty string maps to 'None' which means you probably dont care. In this case
the first character seen by the materials 'FontWithTextures' property is
used for default_caracter.";

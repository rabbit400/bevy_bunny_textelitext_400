use std::ops::Mul;

use ab_glyph::PxScale;
use bevy::prelude::*;
use bevy::reflect::TypePath;
use bevy::reflect::TypeUuid;
use bevy::render::render_resource::AsBindGroup;
use bevy::render::render_resource::Extent3d;
use bevy::render::render_resource::TextureDimension;
use bevy::render::texture::ImageSampler;
use bevy::utils::HashMap;

pub struct TextMaterialPlugin;
impl Plugin for TextMaterialPlugin {
    fn build(&self, app: &mut App) {

        load_internal_assets(app);
    
        app
            .init_asset::<FontWithTextureArray>()
            .add_systems(Update, (
                    update_texts,
                    recreate_font_images,
                    update_texture_handles,
                ).chain()
            )
            ;
    }
}

const TEXT_SHADER_1_HANDLE: Handle<Shader> =  Handle
    ::weak_from_u128(
        9__459_534_112__395_800_278 // random.org, two 9-digit number (i added leading '9')
        + 87_934_598_346 // manual "random" key presses
    );

fn load_internal_assets(app: &mut App)
{
    use bevy::asset::load_internal_asset;

    load_internal_asset!(
        app,
        TEXT_SHADER_1_HANDLE,
        "../assets/text_shader_1.wgsl",
        Shader::from_wgsl
    );
}

// TODO reconsider construction an stuff
// e.g. maybe remove setters for fields that could be public
#[derive(AsBindGroup, TypeUuid, TypePath, Debug, Clone, Asset)]
#[uuid = "610ff684-1713-46dd-967a-ea32630f245e"]
pub struct TextMaterial {
    #[uniform(0)]
    row_length: u32,
    #[uniform(0)]
    num_rows: u32,
    #[uniform(0)]
    text_colour: Color,
    #[uniform(0)]
    background_colour: Color,

    #[texture(1, dimension = "2d_array")]
    #[sampler(2)]
    font_texture: Handle<Image>,

    #[uniform(3)]
    num_chars: u32,
    #[storage(4, read_only)]
    chars: Vec<u32>,

    text_changed : bool,
    text: Vec<char>,

    font_with_textures: Handle<FontWithTextureArray>,
}
impl Material for TextMaterial {
    fn fragment_shader() -> bevy::render::render_resource::ShaderRef {
        TEXT_SHADER_1_HANDLE.into()
    }
    fn alpha_mode(&self) -> AlphaMode {
        AlphaMode::Blend
    }
}
impl TextMaterial {
    pub fn new(
        font_with_textures: Handle<FontWithTextureArray>,
    ) -> Self {
        Self
        {
            num_chars: Default::default(),
            chars: Default::default(),
            row_length: 3,
            num_rows: 1,
            text_colour: Color::BLACK,
            background_colour: Color::LIME_GREEN,
            font_texture: Handle::Weak(AssetId::invalid()),
            text_changed: Default::default(),
            text: Default::default(),
            font_with_textures,
        }
    }
    pub fn set_text(self: &mut Self, text: Vec<char>, row_length: u32, num_rows: u32) {
        self.text = text;
        self.text_changed = true;
        self.row_length = row_length;
        self.num_rows = num_rows;
    }
    pub fn set_text_colour(self: &mut Self, text_colour: Color) {
        self.text_colour = text_colour;
    }
    pub fn set_background_colour(self: &mut Self, background_colour: Color) {
        self.background_colour = background_colour;
    }
}

fn update_texts(
    mut materials: ResMut<Assets<TextMaterial>>,
    mut font_texture_assets: ResMut<Assets<FontWithTextureArray>>,
) {
    for (_, m) in materials.iter_mut()
    {
        if m.text_changed
        {
            if let Some(guard) = font_texture_assets.get_mut(&m.font_with_textures)
            {
                m.chars.clear();
                for c in m.text.iter()
                {
                    let i_char = guard.get_texture_array_index(*c);
                    m.chars.push(i_char as u32);
                }
                m.num_chars = m.chars.len() as u32;
                m.text_changed = false;
            }
        }
    }
}

fn recreate_font_images(
    fonts: Res<Assets<Font>>,
    mut images: ResMut<Assets<Image>>,
    mut materials: ResMut<Assets<TextMaterial>>,
    mut font_w_textures_assets: ResMut<Assets<FontWithTextureArray>>,
) {
    for (_, m) in materials.iter_mut()
    {
        if let Some(guard) = font_w_textures_assets.get_mut(&m.font_with_textures)
        {
            guard.try_recreate_texture_array_if_needed(&fonts, &mut images);
        }
    }
}

fn update_texture_handles(
    mut materials: ResMut<Assets<TextMaterial>>,
    mut font_w_textures_assets: ResMut<Assets<FontWithTextureArray>>,
) {
    for (_, m) in materials.iter_mut()
    {
        if let Some(guard) = font_w_textures_assets.get_mut(&m.font_with_textures)
        {
            if m.font_texture != guard.font_image_handle
            {
                m.font_texture = guard.font_image_handle.clone();
            }
        }
    }
}

#[derive(Asset, TypePath, Debug, Clone)]
pub struct FontWithTextureArray {
    image_width_for_single_character_in_pixels: u8,
    image_height_for_single_character_in_pixels: u8,
    keep_font_aspect_ratio: bool,
    /// should be in [0.0, 1.0)
    /// if set the character image pixels will be either 0 or MAX
    ///     depending on the coverage of that pixel is <= coverage_cutoff
    /// if not set the pixels will have the covarage:f32 value rounded
    /// to the pixels range - currently 0 to 255 since we use R8Unorm
    coverage_cutoff: Option<f32>,
    font_handle: Handle<Font>,
    sampler: Option<ImageSampler>,

    font_image_handle: Handle<Image>,
    num_chars_in_image: usize,
    char_to_index: HashMap<char, usize>,
}
impl FontWithTextureArray
{
    pub fn new(
        image_width_for_single_character_in_pixels: u8,
        image_height_for_single_character_in_pixels: u8,
        keep_font_aspect_ratio: bool,
        coverage_cutoff: Option<f32>,
        font_handle: Option<Handle<Font>>,
        sampler: Option<ImageSampler>,
        default_char: Option<char>,
    ) -> Self {
        let mut ret = Self {
            image_width_for_single_character_in_pixels,
            image_height_for_single_character_in_pixels,
            keep_font_aspect_ratio,
            font_handle: font_handle.unwrap_or(Handle::<Font>::default()),
            sampler,
            coverage_cutoff,
            font_image_handle: Handle::Weak(AssetId::invalid()),
            num_chars_in_image: Default::default(),
            char_to_index: Default::default(),
        };
        if let Some(default_char_) = default_char {
            // this will insert the char with id 0 because it is the first inserted character
            ret.get_texture_array_index(default_char_);
        }
        ret
    }
    pub fn get_texture_array_index(&mut self, c : char) -> usize
    {
        let len = self.char_to_index.len();
        let res = self.char_to_index.try_insert(c, len);
        
        let i_char = match res {
            // the char was already known
            Err(r) => *r.entry.get(),
            // the char was added
            Ok(_) => len,
        };
        i_char
    }

    pub fn need_to_recreate_texture_array(self: &Self) -> bool {
        self.char_to_index.len() > self.num_chars_in_image
    }

    /// returns
    ///   true if the array was recreated
    ///   false if recreation was not needed
    ///   false if font handle is not availible (e.g. not finished loading)
    pub fn try_recreate_texture_array_if_needed(
        self: &mut Self,
        fonts: &Res<Assets<Font>>,
        images: &mut ResMut<Assets<Image>>,
    ) -> bool
    {
        if !self.need_to_recreate_texture_array() {
            return false;
        }
        
        let Some(font) = fonts.get(&self.font_handle)
        else {
            return false;
        };
        
        let n = self.char_to_index.len();
        
        let mut chars = Vec::<char>::new();
        chars.resize(n, 'a');
        
        for (c, i) in self.char_to_index.iter()
        {
            chars[*i] = *c;
        }

        // BUG WORKAROUND we crash if we create a texture array of length 1
        // this is a bug in bevy function 'reinterpret_stacked_2d_as_array'
        // TODO remove workaround if/when bevy fixes the bug
        if chars.len() == 1 { chars.push(chars[0]); }
        let num_layers = chars.len() as u32;

        let mut new_image = create_image_for_characters(
            self.image_width_for_single_character_in_pixels,
            self.image_height_for_single_character_in_pixels,
            self.keep_font_aspect_ratio,
            self.coverage_cutoff,
            &chars,
            font);
        
        new_image.reinterpret_stacked_2d_as_array(num_layers);

        if let Some(sampler) = &self.sampler {
            new_image.sampler = sampler.clone();
        }

        self.font_image_handle = images.add(new_image);
        self.num_chars_in_image = n;

        true
    }
}

fn create_image_for_characters(
    px_width: u8,
    px_height: u8,
    keep_font_aspect_ratio: bool,
    coverage_cutoff: Option<f32>,
    characters: &Vec<char>,
    bevy_font: &Font
) -> Image
{
    let data_size: usize = usize::from(px_width) * usize::from(px_height) * characters.len();
    let mut pixels = Vec::<u8>::new();
    pixels.resize(data_size, 0);

    let coverage_cutoff = coverage_cutoff.map(|f| f.clamp(0.0, 1.0));
    for (i, character) in characters.iter().enumerate()
    {
        let draw_pixel = |x:u32, y:u32, coverage:f32|
        {
            let coverage = coverage.clamp(0.0, 1.0);
            let y_ = y + (i as u32) * u32::from(px_height);
            let index = x + y_ * u32::from(px_width);
            let val = match coverage_cutoff
            {
                Some(cutoff) =>
                    (coverage - cutoff)  // in [-1.0 1.0]
                    .ceil()             // now 1 if coverage > limit
                    .max(0.0)           // now 1 or 0
                    .mul(255.0) as u8,  // now 255 or 0
                None =>
                    (coverage * 255.0).round() as u8,
            };
            pixels[usize::try_from(index).unwrap()] = val;
        };

        draw_a_character(px_width, px_height, keep_font_aspect_ratio, &bevy_font, *character, draw_pixel);
    }

    let image = Image::new(
        Extent3d {
            width: px_width.into(),
            height: u32::from(px_height) * characters.len() as u32,
            depth_or_array_layers: 1,
        },
        TextureDimension::D2,
        pixels,
        bevy::render::render_resource::TextureFormat::R8Unorm
    );

    image
}

/// Draw a character.
///
/// "draw_pixel" is called for each `(x, y)` with coverage > 0
/// where 0 <= x < px_width and 0 <= y < px_height.
/// The float argument "coverage" is in range `[0.0, 1.0]` and
/// represents how much of that pixel is covered by the character.
fn draw_a_character(
    // TODO probably create type for (px_width, px_height, keep_aspect) (maybe CharDrawSizeInfo?)
    px_width: u8,
    px_height: u8,
    keep_aspect_ratio: bool,
    font: &Font,
    character: char,
    mut draw_pixel: impl FnMut(u32, u32, f32)
) {
    // there is nothing to draw
    // AND the font will return None for glyph :/
    if character == ' ' { return; }
    use ab_glyph::Font;
    use ab_glyph::point;

    let f = &font.font;
    
    // Pixel scale. This is the pixel-height of text.
    let scale: PxScale = {
        let w_max_scale = f32::from(px_width);
        let h_max_scale = f32::from(px_height) / f.units_per_em().unwrap() * f.height_unscaled();
        
        if keep_aspect_ratio {
            f32::min(w_max_scale, h_max_scale).into()
        } else {
            PxScale { x: w_max_scale, y: h_max_scale }
        }
    };

    let y_offset: f32 = {
        let height_of_font_in_pixels = scale.y;
        let extra_space = f32::from(px_height) - height_of_font_in_pixels;
        let ratio_of_font_above_the_line = f.ascent_unscaled() / f.height_unscaled();
        extra_space / 2.0
        + height_of_font_in_pixels * ratio_of_font_above_the_line
    };

    let glyph = f.glyph_id(character).with_scale_and_position(
        scale,
        point(0.0, y_offset.into())
    );

    let Some(glyph_outline) = f.outline_glyph(glyph)
        else
        {
            println!("found no outline for '{}'", character);
            return;
        };
    let bounds = glyph_outline.px_bounds();
    
    let y_add_ = u32_try_from(bounds.min.y.max(0.0));
    let y_add = y_add_.unwrap();
    let y_subtract = u32_try_from((-bounds.min.y).max(0.0)).unwrap();

    let x_extra_space = f32::from(px_width) - (bounds.max.x - bounds.min.x);
    let x_offset = (0.5 * x_extra_space).ceil();
    let x_add = u32_try_from(x_offset.max(0.0)).unwrap();
    let x_subtract = u32_try_from((-x_offset).max(0.0)).unwrap();

    let try_get_xy_in_range = |mut x: u32, mut y: u32| -> Option<(u32, u32)>
    {
        if y >= y_subtract && x >= x_subtract
        {
            y = (y - y_subtract) + y_add;
            x = (x -x_subtract) + x_add;
            
            if x < u32::from(px_width) && y < u32::from(px_height)
            {
                return Some((x, y))
            }
        }
        None
    };

    glyph_outline.draw(|x_, y_, cover|
    {
        if let Some(index) = try_get_xy_in_range(x_, y_)
        {
            let (x, y) = index;
            if cover > 0.0 {
                draw_pixel(x, y, cover);
            }
        }
    });
}

pub fn u32_try_from(f:f32) -> Option<u32>
{
    if f >= 0.0
        && f.fract() == 0.0
        && f == (f as u32) as f32
    {
        Some(f as u32)
    }
    else {
        None
    }
}

#import bevy_pbr::mesh_view_bindings::globals
#import bevy_pbr::forward_io::VertexOutput
#import bevy_pbr::utils::coords_to_viewport_uv

struct TextMaterialData {
    row_length: u32,
    num_rows: u32,
    text_colour: vec4f,
    background_colour: vec4f,
}

@group(1) @binding(0)
var<uniform> data : TextMaterialData;
@group(1) @binding(1)
var my_font_texture_array:  texture_2d_array<f32>;
@group(1) @binding(2)
var my_font_texture_sampler: sampler;

@group(1) @binding(3)
var<uniform> num_chars: u32;
@group(1) @binding(4)
var<storage> chars: array<u32>;

@fragment
fn fragment(
    mesh: VertexOutput,
) -> @location(0) vec4<f32> 
{
    let column =    u32(floor(f32(data.row_length) * mesh.uv.x));
    let row =       u32(floor(f32(data.num_rows  ) * mesh.uv.y));
    let x_in_char = fract(f32(data.row_length) * mesh.uv.x);
    let y_in_char = fract(f32(data.num_rows  ) * mesh.uv.y);

    let index_in_string = column + row * data.row_length;
    let string_length = num_chars;// u32(4);
    let char = select(
        u32(0),
        u32(chars[index_in_string]),
        index_in_string < string_length
    );

    let index_in_font_array = select(
        u32(0),
        char,
        char < textureNumLayers(my_font_texture_array)
    );

    let uv = vec2f(x_in_char, y_in_char);

    let font_texture_colour = textureSample(
        my_font_texture_array,
        my_font_texture_sampler,
        uv,
        index_in_font_array);
    
    // my_font_texture_array is assumed to be single red channel.
    // At time of writing it is TextureFormat::R8Unorm set in host rust code.
    let t = font_texture_colour.x;

    var fg_colour = data.text_colour;
    fg_colour.a *= t;

    return add_colours(fg_colour, data.background_colour);
}

fn add_colours(foreground: vec4f, background: vec4f)
-> vec4f
{
    var ret
        = foreground * foreground.a
        + background * background.a * (1.0 - foreground.a);
    ret.a
        = foreground.a
        + background.a * (1.0 - foreground.a);
    return ret;
}
